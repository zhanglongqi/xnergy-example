#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
The app will try to start charging 

and show the charger voltage, battery voltage and charging current 
stop charging in 30 seconds
'''
from minimalmodbus import Instrument, MODE_RTU
import serial
import time

VERSION = '0.1.0'
UNIT = 16

MODBUS_FC_READ_SINGLE_COIL = int("0x01", 16)
MODBUS_FC_READ_SINGLE_DISCRETE = int("0x02", 16)
MODBUS_FC_READ_HOLDING_REGISTERS = int("0x03", 16)
MODBUS_FC_READ_INPUT_REGISTERS = int("0x04", 16)
MODBUS_FC_WRITE_SINGLE_REGISTER = int("0x06", 16)

enable_charger_address = 0
charger_voltage_adddress = 30
batery_voltage_address = 32
current_address = 31

def run():

	import argparse

	parser = argparse.ArgumentParser(description='xnergy charger control demo')

	parser.add_argument('port', type=str,
											help='The serial port(RS485) that connect to xnergy RCU')
	
	# parser.add_argument('-P', '--parallel', type=int, nargs='*', choices=(17, 18, 19),
	# 										help='set to parallel mode, parameter is the unit id, can be used multiple times')

	parser.add_argument('-v', '--verbose', action='count', default=0,
											help='verbose mode, show more information')

	args = parser.parse_args()

	instrument = Instrument(port=args.port, mode=MODE_RTU, slaveaddress=UNIT)
	instrument.serial.baudrate = 9600
	# start charging
	instrument.write_bit(registeraddress=enable_charger_address, value=1)
	print('Start Charging, will stop charging in 30 seconds')

	counter = 0
	while True:
		charger_voltage = instrument.read_registers(registeraddress=charger_voltage_adddress, number_of_registers=1,
																					functioncode=MODBUS_FC_READ_INPUT_REGISTERS)
		battery_voltage = instrument.read_registers(registeraddress=batery_voltage_address, number_of_registers=1,
																					functioncode=MODBUS_FC_READ_INPUT_REGISTERS)
		current = instrument.read_registers(registeraddress=current_address, number_of_registers=1,
																					functioncode=MODBUS_FC_READ_INPUT_REGISTERS)
		
		print(
			str(counter) + ' ' 

			'Charger voltage: ' + str(charger_voltage) + ' '
			'Battery voltage: ' + str(battery_voltage) + ' '
			'Current: ' + str(current)
			, end='\r', flush=True)
		
		counter += 1
		if counter > 30:
			break
		time.sleep(1)
	print()

	# stop charging
	instrument.write_bit(registeraddress=enable_charger_address, value=0)
	print('Stop Charging')


if __name__ == "__main__":
	run()
